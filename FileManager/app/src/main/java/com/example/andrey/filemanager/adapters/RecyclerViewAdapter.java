package com.example.andrey.filemanager.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andrey.filemanager.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<File> mArrData = null;
    private OnItemClickListener mListener = null;
    private OnLongItemClickListener mLongListener = null;

    public RecyclerViewAdapter(ArrayList<File> filesList) {
        mArrData = filesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.recycler_view_activity_main_item, parent, false);
        return new FileViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        File file = mArrData.get(position);
        FileViewHolder fileViewHolder = (FileViewHolder) holder;
        if (file.isDirectory()) {
            fileViewHolder.fileNameTextView.setText(String.format(Locale.US, "%s/", file.getName()));
            fileViewHolder.iconImageView.setImageResource(R.drawable.ic_folder);
        } else {
            String fileName = file.getName();
            String fileType = fileName.substring(fileName.lastIndexOf('.') + 1,
                    fileName.length());
            if (fileType.equalsIgnoreCase("JPG") || fileType.equalsIgnoreCase("PNG")) {
                fileViewHolder.iconImageView.setImageResource(R.drawable.ic_photo);
            } else if (fileType.equalsIgnoreCase("MP3") || fileType.equalsIgnoreCase("OGG")) {
                fileViewHolder.iconImageView.setImageResource(R.drawable.ic_musical_notes);
            } else if (fileType.equalsIgnoreCase("TXT") || fileType.equalsIgnoreCase("DOC")
                    || fileType.equalsIgnoreCase("PDF") || fileType.equalsIgnoreCase("FB2")
                    || fileType.equalsIgnoreCase("RTF")) {
                fileViewHolder.iconImageView.setImageResource(R.drawable.ic_document_text);
            } else {
                fileViewHolder.iconImageView.setImageResource(R.drawable.ic_help);
            }
            fileViewHolder.fileNameTextView.setText(file.getName());
        }

    }

    @Override
    public int getItemCount() {
        return mArrData.size();
    }

    class FileViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewMainActivityRecyclerViewItem)
        ImageView iconImageView = null;
        @BindView(R.id.contentMainActivityRecyclerViewItem)
        TextView fileNameTextView = null;

        FileViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onItemClickListener(fileNameTextView.getText().toString(),
                                FileViewHolder.this.getLayoutPosition());
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mLongListener != null) {
                        mLongListener.onLongItemClickListener(fileNameTextView.getText().toString(),
                                FileViewHolder.this.getLayoutPosition());
                        return true;
                    }
                    return false;
                }
            });
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClickListener(String fileName, int position);
    }

    public interface OnLongItemClickListener {
        void onLongItemClickListener(String fileName, int position);
    }

    public void setOnLongClickListener(OnLongItemClickListener longListener) {
        mLongListener = longListener;
    }

    public void setOnClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public void setData(ArrayList<File> mArrData) {
        this.mArrData = mArrData;
    }
}
