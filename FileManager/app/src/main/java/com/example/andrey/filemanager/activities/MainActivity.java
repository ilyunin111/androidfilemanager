package com.example.andrey.filemanager.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrey.filemanager.R;
import com.example.andrey.filemanager.adapters.RecyclerViewAdapter;
import com.example.andrey.filemanager.subsidiary.Constants;
import com.example.andrey.filemanager.subsidiary.FileComparator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements RecyclerViewAdapter.OnItemClickListener, RecyclerViewAdapter.OnLongItemClickListener,
        SearchView.OnQueryTextListener {

    private static final String LOG_TAG = "Default Log Tag";

    private String mStrTypeFilter = "";
    private String mStrSearchText = "";
    private String mStrRoot = null;
    private String mStrCurrentPath = null;
    private String mStrParentDirectory = null;
    private ArrayList<File> mFullFilesList = null; //Реальные пути файлов в листе стрингов
    private ArrayList<File> mMutableFilesList = null; //Модифируемый лист (т.е. фильтруемый относительно листа всех файлов)

    private SearchView mSearchView = null;
    private RecyclerViewAdapter mAdapter = null;
    private Resources mRes = null;
    private Comparator<? super File> mFilesComparator = FileComparator.getAlphabeticalComparatorInstance();
    private MimeTypeMap mMimeTypeMap = null;
    private PackageManager mPackageManager = null;

    @BindView(R.id.pathToFileMainActivityTextView)
    TextView mPathToFileTextView = null;
    @BindView(R.id.recyclerViewMainActivity)
    RecyclerView mFilesRecyclerView = null;

    //TODO реализовать выделение элементов с анимированием

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mRes = getResources();

        mMimeTypeMap = MimeTypeMap.getSingleton();
        mPackageManager = getPackageManager();

        //mStrRoot = Environment.getExternalStorageDirectory().getPath();
        mStrRoot = "/";
        Log.i(LOG_TAG, "mStrRoot = " + mStrRoot);

        LinearLayoutManager manager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mFilesRecyclerView.setLayoutManager(manager);
        mAdapter = new RecyclerViewAdapter(null);
        mAdapter.setOnClickListener(this);
        mAdapter.setOnLongClickListener(this);

        getDirectoryInformation(mStrRoot, null, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.searchActionMainActivityMenuItem);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        mSearchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortCategoryMainActivityMenuItem:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                dialogBuilder.setTitle(R.string.choose_sort_dialog)
                        .setItems(R.array.sort_variants, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        mFilesComparator = FileComparator.getAlphabeticalComparatorInstance();
                                        getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                                        break;
                                    case 1:
                                        mFilesComparator = FileComparator.getLastModifiedComparatorInstance();
                                        getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                                        break;
                                    case 2:
                                        mFilesComparator = FileComparator.getSizeComparatorInstance();
                                        getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                                        break;
                                    case 3:
                                        mFilesComparator = FileComparator.getFileTypeComparatorInstance();
                                        getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                                        break;
                                }
                            }
                        }).show();
                break;
            case R.id.homeNavigateMainActivityMenuItem:
                String toBackDirectory = null;
                if (!mStrCurrentPath.equals(mStrRoot)) {
                    toBackDirectory = mStrCurrentPath;
                }
                getDirectoryInformation(mStrRoot, null, null);
                mStrParentDirectory = toBackDirectory;
                break;
            case R.id.searchActionMainActivityMenuItem:
                break;
            case R.id.typeFilterMainActivityMenuItem:
                final View typeFilterDialog = getLayoutInflater().inflate(
                        R.layout.dialog_filter_type, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle(R.string.type_filter_main_activity_dialog_title)
                        .setView(typeFilterDialog)
                        .setPositiveButton("Filter", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText typeFilterEditText = (EditText) typeFilterDialog.findViewById(
                                        R.id.typeFilterDialogEditText);
                                mStrTypeFilter = typeFilterEditText.getText().toString();
                                Toast.makeText(MainActivity.this, mStrTypeFilter, Toast.LENGTH_SHORT).show();
                                getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                            }
                        })
                        .setNegativeButton("Cancel", null);
                builder.create().show();
                break;
            case R.id.resetListMainActivityMenuItem:
                mStrTypeFilter = null;
                mStrSearchText = null;
                getDirectoryInformation(mStrCurrentPath, null, null);
                break;
            default:
                Toast.makeText(this, "Action isn't defined.", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mStrParentDirectory != null) {
            getDirectoryInformation(mStrParentDirectory, null, null);
        } else {
            super.onBackPressed();
        }
    }

    public void getDirectoryInformation(String strDirPath, String strSearchText, String strTypeFilter) {
        mStrCurrentPath = strDirPath;
        mPathToFileTextView.setText(String.format(Locale.US,
                mRes.getString(R.string.path_to_file_text_view), strDirPath));
        mFullFilesList = new ArrayList<>();
        mMutableFilesList = new ArrayList<>();
        ArrayList<File> tempFilesList = new ArrayList<>();
        File directoryFile = new File(strDirPath);
        File[] arrFiles = directoryFile.listFiles();
        mStrParentDirectory = null;

        if (!strDirPath.equals(mStrRoot)) {
            mStrParentDirectory = directoryFile.getParent();
        }

        Arrays.sort(arrFiles, mFilesComparator);

        for (File tempFile : arrFiles) {
            if (!tempFile.isHidden() && tempFile.canRead()) {
                mFullFilesList.add(tempFile);
            }
        }

        tempFilesList.addAll(mFullFilesList);

        if (strSearchText != null && !strSearchText.equals("")) {
            for (File file : mFullFilesList) {
                if (!file.getName().toLowerCase().contains(strSearchText.toLowerCase())) {
                    tempFilesList.remove(file);
                }
            }
        }

        if (strTypeFilter != null && !strTypeFilter.equals("")) {
            for (File file : mFullFilesList) {
                if (!file.getName().substring(file.getName().lastIndexOf('.') + 1,
                        file.getName().length()).toLowerCase().contains(strTypeFilter.toLowerCase())) {
                    tempFilesList.remove(file);
                }
            }
        }

        mMutableFilesList.addAll(tempFilesList);
        mAdapter.setData(mMutableFilesList);
        mFilesRecyclerView.setAdapter(mAdapter);
    }

    private String getExifFromImage(ExifInterface exif) {
        return getTagString(ExifInterface.TAG_DATETIME, exif)
                + getTagString(ExifInterface.TAG_FLASH, exif)
                + getTagString(ExifInterface.TAG_GPS_LATITUDE, exif)
                + getTagString(ExifInterface.TAG_GPS_LATITUDE_REF, exif)
                + getTagString(ExifInterface.TAG_GPS_LONGITUDE, exif)
                + getTagString(ExifInterface.TAG_GPS_LONGITUDE_REF, exif)
                + getTagString(ExifInterface.TAG_IMAGE_LENGTH, exif)
                + getTagString(ExifInterface.TAG_IMAGE_WIDTH, exif)
                + getTagString(ExifInterface.TAG_MAKE, exif)
                + getTagString(ExifInterface.TAG_MODEL, exif)
                + getTagString(ExifInterface.TAG_ORIENTATION, exif)
                + getTagString(ExifInterface.TAG_WHITE_BALANCE, exif);
    }

    private String getTagString(String tag, ExifInterface exif) {
        return tag + ": " + exif.getAttribute(tag) + "\n";
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mStrSearchText = query;
        if (mFullFilesList != null) {
            mMutableFilesList = new ArrayList<>();
            ArrayList<File> arrResult = new ArrayList<>();
            String loweredText = query.toLowerCase();
            for (File file : mFullFilesList) {
                if (file.getName().contains(loweredText)) {
                    mMutableFilesList.add(mFullFilesList.get(mFullFilesList.indexOf(file)));
                    arrResult.add(file);
                }
            }
            mAdapter.setData(arrResult);
            mFilesRecyclerView.setAdapter(mAdapter);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mStrSearchText = newText;
        if (mFullFilesList != null) {
            mMutableFilesList = new ArrayList<>();
            ArrayList<File> arrResult = new ArrayList<>();
            String loweredText = newText.toLowerCase();
            for (File file : mFullFilesList) {
                if (file.getName().contains(loweredText)) {
                    mMutableFilesList.add(mFullFilesList.get(mFullFilesList.indexOf(file)));
                    arrResult.add(file);
                }
            }
            mAdapter.setData(arrResult);
            mFilesRecyclerView.setAdapter(mAdapter);
        }
        return true;
    }

    @Override
    public void onLongItemClickListener(final String fileName, int position) {

        final File file = mMutableFilesList.get(position);

        if (!file.exists()) {
            Toast.makeText(this, "This file doesn't exist anymore.", Toast.LENGTH_LONG).show();
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.file_click_choose_action_dialog_title)
                .setItems(R.array.file_long_click_dialog, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (file.delete()) {
                                    Toast.makeText(MainActivity.this, "File has been deleted.", Toast.LENGTH_LONG).show();
                                    getDirectoryInformation(mStrCurrentPath, mStrSearchText, mStrTypeFilter);
                                } else {
                                    Toast.makeText(MainActivity.this, "Unable to delete the file.", Toast.LENGTH_LONG).show();
                                }
                                break;
                            case 1:
                                String exifAttribute = "";
                                String fileType = fileName.substring(fileName.lastIndexOf('.') + 1,
                                        fileName.length());

                                if (fileType.equalsIgnoreCase(Constants.FILE_TYPE_JPG)) {
                                    try {
                                        ExifInterface exif = new ExifInterface(file.toString());
                                        exifAttribute = getExifFromImage(exif);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        Toast.makeText(MainActivity.this, "IOException caught.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }

                                String fileInfo = "Absolute path: " + file.getAbsolutePath()
                                        + "\nPath: " + file.getPath()
                                        + "\nParent: " + file.getParent()
                                        + "\nName: " + file.getName()
                                        + "\nLast update: " + new Date(file.lastModified())
                                        + "\nSize: " + file.length();

                                new AlertDialog.Builder(MainActivity.this)
                                        .setIcon(R.mipmap.ic_launcher)
                                        .setTitle("[" + file.getName() + "]")
                                        .setMessage(fileInfo + " " + exifAttribute)
                                        .setPositiveButton("OK", null)
                                        .show();
                                break;
                        }
                    }
                }).show();


    }

    @Override
    public void onItemClickListener(String fileName, int position) {
        File file = mMutableFilesList.get(position);
        if (file.isDirectory()) {
            mStrSearchText = "";
            if (file.canRead()) {
                getDirectoryInformation(mFullFilesList.get(position).getPath(), null, null);
            } else {
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle("[" + file.getName() + "] directory isn\'t accessible.")
                        .setPositiveButton("OK", null)
                        .show();
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String mimeType = mMimeTypeMap.getMimeTypeFromExtension(getFileMimeType(mMutableFilesList.get(position).getName()));
            intent.setDataAndType(Uri.fromFile(mMutableFilesList.get(position)), mimeType);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            List activities = mPackageManager.queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            if (activities.size() > 0) {
                startActivity(intent);
            } else {
                Toast.makeText(this, "No apps able to open this file have been detected on the device.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private String getFileMimeType(String strUri) {
        int questionMarkIndex = strUri.indexOf("?");
        if (questionMarkIndex > -1) {
            strUri = strUri.substring(0, questionMarkIndex);
        }
        if (strUri.lastIndexOf(".") == -1) {
            return null;
        }
        String strExtension = strUri.substring(strUri.lastIndexOf(".") + 1);
        if (strExtension.contains("%")) {
            strExtension = strExtension.substring(strExtension.indexOf("%"));
        }
        if (strExtension.contains("/")) {
            strExtension = strExtension.substring(0, strExtension.indexOf("/"));
        }
        return strExtension.toLowerCase();
    }
}