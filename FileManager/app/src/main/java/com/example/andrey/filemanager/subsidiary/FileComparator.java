package com.example.andrey.filemanager.subsidiary;

import java.io.File;
import java.util.Comparator;

public class FileComparator {

    private static Comparator<? super File> fileNameComparator = null;
    private static Comparator<? super File> fileLastModifiedComparator = null;
    private static Comparator<? super File> fileSizeComparator = null;
    private static Comparator<? super File> fileTypeComparator = null;

    public static Comparator<? super File> getAlphabeticalComparatorInstance() {
        if (fileNameComparator == null) {
            fileNameComparator = new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    if (file1.isDirectory()) {
                        if (file2.isDirectory()) {
                            return file1.getName().toLowerCase().compareTo(file2.getName()
                                    .toLowerCase());
                        } else {
                            return -1;
                        }
                    } else {
                        if (file2.isDirectory()) {
                            return 1;
                        } else {
                            return file1.getName().toLowerCase().compareTo(file2.getName()
                                    .toLowerCase());
                        }
                    }
                }
            };
        }
        return fileNameComparator;
    }

    public static Comparator<? super File> getLastModifiedComparatorInstance() {
        if (fileLastModifiedComparator == null) {
            fileLastModifiedComparator = new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    if (file1.isDirectory()) {
                        if (file2.isDirectory()) {
                            return Long.valueOf(file1.lastModified()).compareTo(
                                    file2.lastModified());
                        } else {
                            return -1;
                        }
                    } else {
                        if (file2.isDirectory()) {
                            return 1;
                        } else {
                            return Long.valueOf(file1.lastModified()).compareTo(
                                    file2.lastModified());
                        }
                    }
                }
            };
        }
        return fileLastModifiedComparator;
    }

    public static Comparator<? super File> getSizeComparatorInstance() {
        if (fileSizeComparator == null) {
            fileSizeComparator = new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    if (file1.isDirectory()) {
                        if (file2.isDirectory()) {
                            return file1.getName().toLowerCase().compareTo(file2.getName()
                                    .toLowerCase());
                        } else {
                            return -1;
                        }
                    } else {
                        if (file2.isDirectory()) {
                            return 1;
                        } else {
                            return (int) (file1.length() - file2.length());
                        }
                    }
                }
            };
        }
        return fileSizeComparator;
    }

    public static Comparator<? super File> getFileTypeComparatorInstance() {
        if (fileTypeComparator == null) {
            fileTypeComparator = new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    if (file1.isDirectory()) {
                        if (file2.isDirectory()) {
                            return file1.getName().toLowerCase().compareTo(file2.getName()
                                    .toLowerCase());
                        } else {
                            return -1;
                        }
                    } else {
                        if (file2.isDirectory()) {
                            return 1;
                        } else {
                            String firstFileName = file1.getName();
                            String firstFileType = firstFileName.substring(firstFileName
                                    .lastIndexOf('.') + 1, firstFileName.length());
                            String secondFileName = file2.getName();
                            String secondFileType = secondFileName.substring(secondFileName
                                    .lastIndexOf('.') + 1, secondFileName.length());
                            return firstFileType.toLowerCase().compareTo(secondFileType.toLowerCase());
                        }
                    }
                }
            };
        }
        return fileTypeComparator;
    }
}